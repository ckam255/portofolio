const DagTourism = () => {
    return <li className="timeline_element project">
        <div className="timeline_element-date">
            <time className="timeline_element-date-text">2019-2020</time>
        </div>

        <div className="timeline_element-contents">
            <div className="project-text">
                <a href="#" className="project-link">
                    <div className="project-title">DagTourism</div>
                    <div className="project-subtitle">
                        An application mobile for tourism in Dagestan precisely the
                        city of derbent.
</div>
                </a>
                <div className="project-description">
                    <p>
                        <em></em>
                    </p>

                    <p>Whole project includes:</p>

                    <ul>
                        <li>
                            Registration of tourist and historical points, restaurants
                            and 5 star hotels, attraction points.
</li>
                        <li>
                            Visualization of tourist points on a map integrated into the mobile application.
</li>
                        <li>
                            Visualization of tourist points on a map integrated into the
                            mobile application.
</li>
                        <li>
                            Possibility to navigate from its position to the designated
                            tourist point
</li>
                        <li>Integration of 360 degree images</li>
                        <li>Find a point through barcode reading</li>
                    </ul>
                </div>

                {/*  <a
                href="https://play.google.com/store/apps/details?id=ooo.cron.dagestan"
                target="_blank" rel="noreferrer"
                className="project-readmore button button-red"
            >
                More details
            </a> */}
                <a
                    href="https://play.google.com/store/apps/details?id=ooo.cron.dagestan"
                    className="button button-red"
                    target="_blank" rel="noreferrer"
                >
                    <i className="fa fa-external-link"></i>
View online
</a>

                <div className="project-technologies">
                    <div className="technologies-title">Technologies</div>
                    <ul className="tech-tags">
                        <li>
                            <a href="https://python.org" rel="nofollow" target="_blank">Python</a>
                        </li>

                        <li>
                            <a
                                href="http://www.doctrine-project.org/"
                                rel="nofollow"
                                target="_blank"
                            >Django Rest framework</a>
                        </li>
                        <li>
                            <a href="https://android.com" rel="nofollow" target="_blank">Android</a>
                        </li>
                        <li>
                            <a href="https://kotlin.org" rel="nofollow" target="_blank">Kotlin</a>
                        </li>
                        <li>
                            <a href="https://java.org" rel="nofollow" target="_blank">Java</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div className="project-image">
                <a href="#">
                    <img src="/img/15.webp" alt="Dagtourism" />
                </a>
            </div>
        </div>
    </li>

}

export default DagTourism;