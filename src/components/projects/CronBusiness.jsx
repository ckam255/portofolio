const CronBusiness = () => {
    return <li className="timeline_element project">
        <div className="timeline_element-date">
            <time className="timeline_element-date-text">2019-2020</time>
        </div>

        <div className="timeline_element-contents">
            <div className="project-text">
                <a href="#" className="project-link">
                    <div className="project-title">CRON BUSINESS</div>
                    <div className="project-subtitle">CRON BUSINESS is online cashbox, associated with a CRM</div>
                </a>
                <div className="project-description">
                    <p>
                        <em></em>
                    </p>

                    <p>Whole project includes:</p>

                    <ul>
                        <li>Opening and closing of cash register</li>
                        <li>
                            Management of sales, returns and Reapprovisionnement of
                            products
                    </li>
                        <li>Search for product by name, barcode</li>
                        <li>Tool for monitoring sales and products.</li>
                        <li>Accounting</li>
                    </ul>
                </div>

                {/* <!-- <a
                href="https://business.cron.ooo/"
                target="_blank"
                className="project-readmore button button-red"
            >
                More details
</a>--> */}

                <a href="https://business.cron.ooo/" className="button button-red" target="_blank" rel="noreferrer">
                    <i className="fa fa-external-link"></i>
View online
</a>

                <div className="project-technologies">
                    <div className="technologies-title">Technologies</div>
                    <ul className="tech-tags">
                        <li>
                            <a href="https://python.org" rel="nofollow" target="_blank" rel="noreferrer">Python</a>
                        </li>

                        <li>
                            <a
                                href="http://www.doctrine-project.org/"
                                rel="nofollow"
                                target="_blank" rel="noreferrer"
                            >Django Rest framework</a>
                        </li>
                        <li>
                            <a href="https://vuejs.org" rel="nofollow" target="_blank">Vue js</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div className="project-image">
                <a href="#">
                    <img src="img/6.png" alt="cronbusiness" />
                </a>
            </div>
        </div>
    </li>

}

export default CronBusiness;