const AlphaCLI = () => {
    return <li className="timeline_element project">
        <div className="timeline_element-date">
            <time className="timeline_element-date-text">2014 - 2016</time>
        </div>

        <div className="timeline_element-contents">
            <div className="project-text">
                <a href="#" className="project-link">
                    <div className="project-title">ALPHA-CLI</div>
                    <div className="project-subtitle">
                        {/* <!-- VoIP solution integrator: implementation of an IP channel
communication platform with the ability to display the caller--> */}
                    </div>
                </a>
                <div className="project-description">
                    <p>
                        <strong>
                            ALPHA-CLI is a VoIP solution integrator: implementation of
                            an IP channel communication platform with the ability to
                            display the caller. It serves both as:
</strong>
                    </p>

                    <ul>
                        <li>
                            Management interface for output numbers , allowing users to
                            add numbers and associated CallerID
</li>
                        <li>Server monitoring system based on Zabbix</li>
                        <li>System allowing reportings bugs to developers</li>
                        <li>Feedback user experience</li>
                        <li>
                            Visualization of calls, statistics through a fluid
                            application based on graphs.
</li>
                    </ul>
                </div>

                {/* <!-- <a
                href="/portfolio/alphacli/"
                className="project-readmore button button-red"
            >
                More details
</a>--> */}

                <div className="project-technologies">
                    <div className="technologies-title">Technologies</div>
                    <ul className="tech-tags">
                        <li>
                            <a href="http://php.net/" rel="nofollow" rel="noreferrer" target="_blank">PHP</a>
                        </li>

                        <li>
                            <a href="https://jquery.com/" rel="nofollow" target="_blank">Jquery</a>
                        </li>

                        <li>
                            <a
                                href="https://dotnet.microsoft.com/apps/aspnet"
                                rel="nofollow"
                                target="_blank"
                            >ASP.NET</a>
                        </li>

                        <li>
                            <a href="http://asterisk.fr/" rel="nofollow" target="_blank">Asterisk</a>
                        </li>

                        <li>
                            <a href="https://www.freepbx.org/" rel="nofollow" target="_blank">Free PBX</a>
                        </li>

                        <li>
                            <a href="https://www.zabbix.org/" rel="nofollow" target="_blank">Zabbix</a>
                        </li>

                        <li>
                            <a
                                href="https://docs.microsoft.com/en-us/dotnet/csharp/"
                                rel="nofollow"
                                target="_blank"
                            >C#</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div className="project-image">
                <a href="#">
                    <img src="/img/14.jpg" alt="Alphacli" />
                </a>
            </div>
        </div>
    </li>

}

export default AlphaCLI;