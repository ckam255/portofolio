import Link from "next/link";

const Freelance = () => {
    return <li className="timeline_element timeline_element--now project">
        <div className="timeline_element-date">
            <time className="timeline_element-date-text">2016 - 2018</time>
        </div>

        <div className="timeline_element-contents">
            <div className="project-text">
                <div className="project-description">
                    During this time I took a temporary break from freelancing.
                    Instead, I was an employee in medium-size tech companies. This
                    gave me a lot of experience and hindsight on how a proper team
                    collaboration, recruitment, and project management process
                    should look like. See
<a
                        href="https://rostov.hh.ru/applicant/resumes/view?resume=9fb4743fff05d850dd0039ed1f4e5a42534d6a"
                    >my CV</a>
                    <Link href="/cv">
                        <a>my CV</a>
                    </Link>
for more details.
</div>
            </div>
        </div>
    </li>
}

export default Freelance;