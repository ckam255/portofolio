const Marketplace = () => {
    return <li className="timeline_element project">
        <div className="timeline_element-date">
            <time className="timeline_element-date-text">Now</time>
        </div>

        <div className="timeline_element-contents">
            <div className="project-text">
                <a href="#" className="project-link">
                    <div className="project-title">05.ru MARKETPLACE</div>
                    <div
                        className="project-subtitle"
                    >It is a web platform where product or service information is provided by 05.ru partners</div>
                </a>
                <div className="project-description">
                    <p>
                        <em>
                            During this time I took a temporary break from freelancing.
                            Instead, I was an employee in medium-size tech companies. This
                            gave me a lot of experience and hindsight on how a proper team
                            collaboration, recruitment, and project management process
                            should look like. See
                        </em>
                    </p>

                    <p>Whole project includes:</p>

                    <ul>
                        <li>Products management</li>
                        <li>Intuitive dashboard</li>
                        <li>Financial Analytics</li>
                        <li>Sales and accounting reports</li>
                        <li>Import and export of products by Excel</li>
                        <li>Synchronization datas between web store</li>
                    </ul>
                </div>

                {/* <!-- <a
                    href="https://mp.05.ru/"
                    target="_blank"
                    className="project-readmore button button-red"
                >
                    More details
</a>--> */}
                <a href="https://mp.05.ru/" className="button button-red" target="_blank">
                    <i className="fa fa-external-link"></i>
View online
</a>

                <div className="project-technologies">
                    <div className="technologies-title">Technologies</div>
                    <ul className="tech-tags">
                        <li>
                            <a href="https://php.net" rel="nofollow" target="_blank">PHP</a>
                        </li>
                        <li>
                            <a href="https://laravel.com" rel="nofollow" target="_blank">Laravel</a>
                        </li>
                        <li>
                            <a href="https://python.org" rel="nofollow" target="_blank">Python</a>
                        </li>

                        <li>
                            <a
                                href="https://www.djangoproject.com/"
                                rel="nofollow"
                                target="_blank"
                            >Django Rest framework</a>
                        </li>
                        <li>
                            <a href="https://1c.ru" rel="nofollow" target="_blank">1C</a>
                        </li>
                        <li>
                            <a href="https://www.vuejs.org" rel="nofollow" target="_blank">Vue JS</a>
                        </li>
                    </ul>
                </div>
            </div>

            <div className="project-image">
                <a href="#">
                    <img src="/img/5.png" alt="marketplace05" />
                </a>
            </div>
        </div>
    </li>

}

export default Marketplace;