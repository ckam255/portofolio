import { BaseLink } from './NavItem';
import portrait from '../../assets/img/portrait.jpg'
import { skills } from '../../helpers'


const Sidebar = () => {
    return <aside className="site-sidebar">
        <div className="site-sidebar-inner h-card">
            <div className="person u-url">
                <BaseLink to="/" exact rel="me">
                    <div className="person-avatar u-photo">
                        <img src={portrait} alt="avatar" className="avatar" />
                    </div>
                </BaseLink>
                <div className="person-content">
                    <h1 className="person-title p-name">Claver Amon</h1>
                    <h2 className="person-subtitle p-job-title p-note">Full-stack Software Developer</h2>

                </div>
            </div>

            <div className="skills-content">
                <div><strong>Strongest skills:</strong></div>
                <div className="skills">
                    {skills.map((skill, i) => <em className="skill" key={i}>{skill}</em>)}
                </div>
            </div>

            <nav className="block main-navigation">
                <div className="navigation-extendable">
                    <ul>
                        <li>
                            <BaseLink activeClass="current" exact to="/">Portfolio</BaseLink>
                        </li>
                        <li >
                            <BaseLink activeClass="current" to="/contact">Contact me</BaseLink>
                        </li>
                        <li >
                            <BaseLink activeClass="current" to="/cv" target="_blank">My CV</BaseLink>
                        </li>
                    </ul>
                </div>
            </nav>

            <div className="block block-social">
                <div className="block-title">Get in touch</div>
                <div className="block-content">
                    <div className="social-icons">
                        <a
                            rel="me"
                            href="mailto:ckam225@gmail.com"
                            className="button button-icon u-email"
                            title="mail: ckam225@gmail.com"
                            target="_blank"
                        >
                            <i className="fa fa-envelope"></i>
                        </a>

                        <a
                            rel="me"
                            href="https://www.linkedin.com/in/claveramon"
                            className="button button-icon"
                            title="linkedin: claveramon"
                            target="_blank"
                        >
                            <i className="fa fa-linkedin-square"></i>
                        </a>

                        <a
                            rel="me"
                            href="https://github.com/msi89"
                            className="button button-icon"
                            title="github: msi89"
                            target="_blank" rel="noreferrer"
                        >
                            <i className="fa fa-github-square"></i>
                        </a>

                        <a
                            rel="me"
                            href="https://twitter.com/msic225"
                            className="button button-icon"
                            title="twitter: @msic225"
                            target="_blank" rel="noreferrer"
                        >
                            <i className="fa fa-twitter-square"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </aside>
}

export default Sidebar;