import React from 'react'
import Sidebar from '../shared/Sidebar'
import { Head } from './Base'

const Container = ({ children }) => {
    return <div className="parent-portfolio">
        <Sidebar />
        <div className="site-main">
            <div className="site-main-inner">
                {children}
            </div>
        </div>
    </div>
}

const MainLayout = ({ children }) => {
    return <>{children}</>
}

MainLayout.Head = Head
MainLayout.Container = Container

export default MainLayout