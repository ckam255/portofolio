
import React from 'react';
import './styles.css'

const Curiculum = () => {
    return <div className="cv">
        <div className="cv_page">
            <div className="cv_header">
                <div>
                    <div className="cv_name">Claver Amon</div>
                    <div className="cv_job_title">Full Stack Developer</div>
                    <div className="cv_contacts">
                        <div className="cv_contact_item">
                            <span class="material-icons">phone</span>
                            +7 989 871 3579
                        </div>
                        <div className="cv_contact_item">
                            <span class="material-icons">alternate_email</span>
                            ckam225@gmail.com
                        </div>
                        <div className="cv_contact_item">
                            <span class="material-icons">link</span>
                            <a href="https://github.com/msi89">https://github.com/msi89</a>
                        </div>
                        <div className="cv_contact_item">
                            <span class="material-icons">location_on</span>
                            Moscow, RU
                        </div>
                    </div>
                </div>
                <div className="cv_about_me">
                    7 years of experience in back-end and front-end engineering. Ambitious and communicative.
                    Good mentor and tech lead.
                    I love C# and python. I am currently interested in Data Sciences
                    projects. However, I am also not closed to other types of projects.
                </div>
            </div>

            <div className="cv_content">

                <div className="cv_panel_1">
                    <div className="cv_heading">EXPERIENCE</div>
                    <div className="cv_experience">
                        <div className="cv_heading_2">Team Lead Full-stack Developer</div>
                        <div className="cv_heading_3">05.RU</div>
                        <div className="cv_heading_4">
                            <div className="cv_heading_icon">
                                <span class="material-icons">schedule</span>
                                2020 - Ongoing
                            </div>
                            <div className="cv_heading_icon">
                                <span class="material-icons">location_on</span>
                                Moscow, RU
                            </div>
                        </div>
                        <div className="" style={{ marginTop: '10px' }}>
                            05.RU is an E-commerce company. It offers you a wide range of electronics and home appliances.
                        </div>
                        <ul className="cv_list_item">
                            <li>Developed api service architecture(repository pattern)</li>
                            <li>Developed 80% code-base of all functionality</li>
                            <li>Configured pipeplines for CI/CD</li>
                            <li>Configured Asynchronous tasks(queue messaging)</li>
                            <li>Bootstrapped, developed and participate most of the decisions throughout the projec</li>
                            <li>Reviewer codes</li>
                            <li>Decomposed tasks on gitlab, trello and now on clickup</li>
                            <li>Participated in planning for the project and team-lead rallies in the development department.</li>
                        </ul>
                    </div>

                    <div className="cv_experience">
                        <div className="cv_heading_2">Full Stack Developer</div>
                        <div className="cv_heading_3">CRON LTD</div>
                        <div className="cv_heading_4">
                            <div className="cv_heading_icon">
                                <span class="material-icons">schedule</span>
                                July 2019 - May 2020
                            </div>
                            <div className="cv_heading_icon">
                                <span class="material-icons">location_on</span>
                                Makhachkala, RU
                            </div>
                        </div>
                        <div className="" style={{ marginTop: '10px' }}>
                            Softwares development, creation of websites and mobile applications of any complexity. <br />
                            Warehouse accounting system
                        </div>
                        <ul className="cv_list_item">
                            <li>Made front-end architecture (file structure: layouts, middlewares,
                                components, pages, split front end into 2 services (cashiers and
                                managers).
                            </li>
                            <li>Developed Cross plateform Authentication between cashiers  and manager services)</li>
                            <li>Developed financial analytics: backend and frontend </li>
                            <li>Developed shared UI components for frontend </li>
                            <li>Participation in mobile application: Dagestan Tourism and Derbent city(JAVA-ANDROID)</li>
                            <li>Developed interface for adding, removing stores, employees,ustomers, cash registers</li>
                            <li>Participated in planning for the project and team-lead rallies in the development department.</li>
                        </ul>
                    </div>

                    <div className="cv_experience">
                        <div className="cv_heading_2">Software Engineer</div>
                        <div className="cv_heading_3">NG SOLUTIONS</div>
                        <div className="cv_heading_4">
                            <div className="cv_heading_icon">
                                <span class="material-icons">schedule</span>
                                Nov 2014 - Sept 2016
                            </div>
                            <div className="cv_heading_icon">
                                <span class="material-icons">location_on</span>
                                Abidjan, CI
                            </div>
                        </div>
                        <div className="" style={{ marginTop: '10px' }}>
                            Telecommunications, Communications
                            • Satellite Communications
                            • Fiber Optic Communications
                            • Fixed Communications
                            • Mobile Communications

                            <div>
                                Integrator of VoIP solutions: implementation of an IP channel
                                communication platform with caller display capability.
                            </div>
                        </div>
                        <ul className="cv_list_item">
                            <li>Participated in the installation and interconnection of equipment to customer networks.</li>
                            <li>Created a call management application(PHP/MySQL) with connected to Free PBX, Asterisk and CDR Report</li>
                            <li>Created a simplified version of Zabbix in php for server supervision via a more user-friendly interface</li>
                            <li>Created and maintained the documentation and build/deploy setup with multiple environments throughout the whole duration of the project.</li>
                        </ul>
                    </div>

                </div>
                <div className="cv_panel_2">
                    <div className="cv_heading">EDUCATION</div>
                    <div className="cv_educations">
                        <div className="cv_edu">
                            <div className="cv_heading_2">Computer security</div>
                            <div className="cv_heading_3">Daghestan State University</div>
                            <div className="cv_heading_4">
                                <div className="cv_heading_icon">
                                    <span class="material-icons">schedule</span>
                                    2017 - 2021
                                </div>
                            </div>
                        </div>
                        <div className="cv_edu">
                            <div className="cv_heading_2">Software Engeering</div>
                            <div className="cv_heading_3">Institut of Technologies Abidjan</div>
                            <div className="cv_heading_4">
                                <div className="cv_heading_icon">
                                    <span class="material-icons">schedule</span>
                                    2011 - 2015
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="cv_heading">SKILLS</div>
                    <div className="cv_skills">

                        <div className="cv_skills_group">
                            <div className="cv_heading_3">Backend</div>
                            <span className="cv_skill_item">Python</span>
                            <span className="cv_skill_item">Django Rest Framework</span>
                            <span className="cv_skill_item">FastAPI</span>
                            <span className="cv_skill_item">PyTest</span>
                            <span className="cv_skill_item">Celery</span>
                            <span className="cv_skill_item">Redis</span>
                            <span className="cv_skill_item">RabbitMQ</span>

                            <span className="cv_skill_item">PHP 7</span>
                            <span className="cv_skill_item">Laravel 8</span>
                            <span className="cv_skill_item">PHPUnit</span>
                            <span className="cv_skill_item">MySQL</span>
                            <span className="cv_skill_item">Postgresql</span>
                        </div>

                        <div className="cv_skills_group">
                            <div className="cv_heading_3">Frontend</div>
                            <span className="cv_skill_item">Javascript</span>
                            <span className="cv_skill_item">Typescript</span>
                            <span className="cv_skill_item">React(Hooks)</span>
                            <span className="cv_skill_item">RecoilJS</span>
                            <span className="cv_skill_item">Redux</span>
                            <span className="cv_skill_item">Vue</span>
                            <span className="cv_skill_item">Vuex</span>
                            <span className="cv_skill_item">VueRouter</span>
                            <span className="cv_skill_item">Jest</span>
                            <span className="cv_skill_item">HTML</span>
                            <span className="cv_skill_item">CSS</span>
                            <span className="cv_skill_item">Bootstrap</span>
                            <span className="cv_skill_item">TailwindCSS</span>
                        </div>

                        <div className="cv_skills_group">
                            <div className="cv_heading_3">DevOps</div>
                            <span className="cv_skill_item">Docker</span>
                            <span className="cv_skill_item">Gitlab CI/CD</span>
                        </div>

                        <div className="cv_skills_group">
                            <div className="cv_heading_3">Miscellaneous </div>
                            <span className="cv_skill_item">Git</span>
                            <span className="cv_skill_item">C#</span>
                            <span className="cv_skill_item">.NET</span>
                            <span className="cv_skill_item">WPF</span>
                            <span className="cv_skill_item">Java</span>
                            <span className="cv_skill_item">Android</span>
                            <span className="cv_skill_item">Sprint boot</span>
                        </div>

                    </div>

                    <div className="cv_heading">LANGUAGES</div>
                    <div className="cv_languages">
                        <div><span className="cv_lang">French</span> — Native</div>
                        <div><span className="cv_lang">Russian</span> — C1 — Advanced</div>
                        <div><span className="cv_lang">English</span>  — B1 — Intermediate</div>
                        <div><span className="cv_lang">German</span> — A1 — Basic</div>

                    </div>
                </div>

            </div>
        </div>
    </div>
}

export default Curiculum;