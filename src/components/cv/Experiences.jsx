const Experiences = () => {
    return <div className="cv-section">
        <h3 className="cv-section-title">Experience</h3>
        <div className="cv-section-content">
            <div className="cv-timeline">
                {/* <!-- marketplace --> */}
                <div className="cv-timeline-row p-experience h-event">
                    <div className="cv-timeline-side">
                        <div className="cv-timeline-company">05.ru</div>
                        <div className="cv-timeline-date">Aug 2020</div>
                        <a href="https://05.ru" target="_blank" rel="noreferrer" className="cv-timeline-website">05.ru</a>
                    </div>
                    {/* <!-- marketplace --> */}
                    <div className="cv-timeline-body p-summary">
                        <h4 className="cv-timeline-position p-name">Lead Software Engineer</h4>
                        <p>
                            Led development of a new back-end for one of the biggest
                            project(marketplace) of the company in the E-commerce and recruiting
                            industry.
                    </p>
                        <ul>
                            <li>
                                Bootstrapped, developed and participate most of the decisions
                                throughout the project. Configured the CI/CD pipeline.
    </li>
                            <li>Created and maintained the documentation and build/deploy of the project.</li>
                            <li>
                                Helped onboard and mentor new team members, both
                                in back-end and the front-end.
    </li>
                            <li>
                                Convinced the upper management to let the team use best
                                available tools (e.g. TypeScript, Vue, Laravel,
                                automatic accessibility/visual/unit tests).
    </li>
                            <li>
                                Actively helped manage, estimate and scope out the
                                project.
    </li>
                        </ul>
                    </div>
                </div>
                {/* <!-- cron business --> */}
                <div className="cv-timeline-row p-experience h-event">
                    <div className="cv-timeline-side">
                        <div className="cv-timeline-company">ooo cron</div>
                        <div className="cv-timeline-date">Sep 2017 - Jun 2018</div>
                        <a href="http://cron.ooo" target="_blank" rel="noreferrer" className="cv-timeline-website">cron.ooo</a>
                    </div>
                    <div className="cv-timeline-body p-summary">
                        <h4 className="cv-timeline-position p-name">Full-stack Software Engineer</h4>
                        <p>Built an entire online cashbox and CRM platform.</p>
                        <ul>
                            <li>Created and launched a large Single Page Application (in Vue and Vuex) that allows partners to manage their employees, products, sales, and more.</li>
                            <li>Created custom VueJS exportables components(UI Kits)</li>
                            <li>Setup entiry project's architecture for front-end(VueJs, VueRouter, Vuex)</li>
                            <li>Created entiry dashboard/analytics from back-end to front-end in Python/Django rest framework and VueJs/ChartJS.</li>
                            <li>Unit tests(Jest)</li>
                            <li>Participated in development and released a mobile app for the tourism in Dagestan. (in Java/Kotlin for Android)</li>
                        </ul>
                    </div>
                </div>
                {/* <!-- freelance --> */}
                <div className="cv-timeline-row p-experience h-event">
                    <div className="cv-timeline-side">
                        <div className="cv-timeline-company">Freelance</div>
                        <div className="cv-timeline-date">November 2016 - May 2019</div>
                    </div>
                    <div className="cv-timeline-body p-summary">
                        <h4 className="cv-timeline-position p-name">Full-stack Software Engineer</h4>
                        <ul>
                            <li>
                                Created multiple web applications, participating in the
                                whole process of their development: product design and
                                estimation, code design and development, DevOps, UI/UX
                                design, product launch and maintenance.
    </li>
                            <li>Mentored several developers and teams.</li>
                        </ul>
                    </div>
                </div>

                <div className="cv-timeline-row p-experience h-event">
                    <div className="cv-timeline-side">
                        <div className="cv-timeline-company">NG SOLUTIONS</div>
                        <div className="cv-timeline-date">Nov 2014 - Sept 2016</div>
                        <a
                            href="https://ngsolutions.net"
                            target="_blank" rel="noreferrer"
                            className="cv-timeline-website"
                        >ngsolutions.net</a>
                    </div>
                    <div className="cv-timeline-body p-summary">
                        <h4 className="cv-timeline-position p-name">Backend Software Engineer</h4>
                        <p>
                            Built and maintained a VoIP platform, that lets companies
                            easily map a callerID to caller number for external communication with there customers.
  </p>
                        <ul>
                            <li>Participated in the installation and interconnection of equipment to customer networks.</li>
                            <li>Created a call management application(PHP/MySQL) with connected to Free PBX, Asterisk and CDR Report</li>
                            <li>Created a simplified version of Zabbix in php for server supervision via a more user-friendly interface</li>
                            <li>Created and maintained the documentation and build/deploy setup with multiple environments throughout the whole duration of the project.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
}

export default Experiences;