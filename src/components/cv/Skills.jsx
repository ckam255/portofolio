const Skills = () => {
    return <div className="cv-section">
        <h3 className="cv-section-title">Skills</h3>
        <div className="cv-section-content cv-section-content--indented">
            <ul style={{ marginTop: 0 }}>
                <li>
                    Back-end: Python(Django Rest Framework, FastAPI), PHP(Laravel), SQL databases, in-memory databases
                    (Redis, RabbitMQ), RabbitMQ, microservice and
                    event-based architectures.
                </li>
                <li>
                    Front-end: JavaScript, TypeScript, React (with Hooks), Vue,
                    Redux, Vuex, Nuxt.js
                </li>
                <li>
                    DevOps: configured and maintained web servers through, Docker,
                    SSH, Gitlab CI/CD.
                </li>
                <li>
                    Attentive to UI/UX details and high code quality. Fan of unit
                    tests, functional programming and typed interfaces.
                </li>
                <li>Open to evolving in: PHP, Python, Docker, and many else.</li>
            </ul>
        </div>
    </div>
}

export default Skills;