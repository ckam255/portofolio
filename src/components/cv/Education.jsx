const Education = () => {
    return <div className="cv-section">
        <h3 className="cv-section-title">Education</h3>
        <div className="cv-section-content">
            <div className="cv-timeline">
                <div className="cv-timeline-row p-education h-event">
                    <div className="cv-timeline-side">
                        <div className="cv-timeline-company">Dagesthan state university</div>
                        {/* <div className="cv-timeline-date">2016 - 2021</div> */}
                        <a href="http://dgu.ru/" target="_blank" rel="noreferrer" className="cv-timeline-website">dgu.ru</a>
                    </div>
                    <div className="cv-timeline-body p-summary">
                        <div className="cv-timeline-position p-name">Informations security</div>
                        <ul>
                            <li>Softwares programming</li>
                            <li>Network and OS security</li>
                            <li>Cryptography</li>
                            <li>More</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        {/* <!-- ITA --> */}
        <div className="cv-section-content">
            <div className="cv-timeline">
                <div className="cv-timeline-row p-education h-event">
                    <div className="cv-timeline-side">
                        <div className="cv-timeline-company">Abidjan Institute of Technologies</div>
                        {/* <div className="cv-timeline-date">2011 - 2015</div> */}
                        <a
                            href="http://www.ita-education.ci/"
                            target="_blank" rel="noreferrer"
                            className="cv-timeline-website"
                        >ita-education.ci</a>
                    </div>
                    <div className="cv-timeline-body p-summary">
                        <div className="cv-timeline-position p-name">Computer Science</div>
                        <ul>
                            <li>Softwares programming</li>
                            <li>Mathematics</li>
                            <li>Computer and telecommunication networks.</li>
                            <li>Data analysis methods</li>
                            <li>More</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

}

export default Education;