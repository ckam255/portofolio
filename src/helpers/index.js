import marketplace from '../assets/img/5.jpg'
import dagtourism from '../assets/img/15.webp'
import cronbusiness from '../assets/img/6.jpg'
import alphacli from '../assets/img/14.jpg'

export function assets(src) {
    //return import(src).then(img => img)
    const requestImageFile = require(src);
    return requestImageFile
}

export const skills = [
    'Python',
    'Django',
    'FastAPI',
    'PHP',
    'Laravel',
    'Javascript',
    'Typescript',
    'React',
    'Vue',
    'TailwindCSS'
]

export const projects = [
    {
        date: 'Now',
        title: '05.ru MARKETPLACE',
        subtitle: 'It is a web platform where product or service information is provided by 05.ru partners',
        description: '',
        image: marketplace, //from public folder
        url: 'https://mp.05.ru',
        features: [
            'Products management',
            'Intuitive dashboard',
            'Financial Analytics',
            'Sales and accounting reports',
            'Import and export of products by Excel',
            'Synchronization datas between web store'
        ],
        skills: [
            { url: 'https://php.net', name: 'PHP' },
            { url: 'https://laravel.com', name: 'LARAVEL' },
            { url: 'https://python.org', name: 'PYTHON' },
            { url: 'https://www.djangoproject.com/', name: 'DJANGO REST FRAMEWORK' },
            { url: 'https://1c.ru', name: '1C' },
            { url: 'https://www.vuejs.org', name: 'VUEJS' },
            { url: 'https://reactjs.org/', name: 'REACT' }
        ]
    },
    {
        date: '2019-2020',
        title: 'DagTourism',
        subtitle: '  An application mobile for tourism in Dagestan precisely the city of derbent.',
        description: '',
        image: dagtourism, //from public folder
        url: 'https://play.google.com/store/apps/details?id=ooo.cron.dagestan',
        features: [
            `Registration of tourist and historical points, restaurants and 5 star hotels, attraction points.`,
            'Visualization of tourist points on a map integrated into the mobile application.',
            'Visualization of tourist points on a map integrated into the mobile application.',
            ' Possibility to navigate from its position to the designated tourist point.',
            'Integration of 360 degree images',
            'Find a point through barcode reading'
        ],
        skills: [
            { url: 'https://python.org', name: 'PYTHON' },
            { url: 'https://www.djangoproject.com/', name: 'DJANGO REST FRAMEWORK' },
            { url: 'https://android.com', name: 'ANDROID' },
            { url: 'https://kotlin.org', name: 'Kotlin' },
            { url: 'https://java.org', name: 'Java' }
        ]
    },
    {
        date: '2019-2020',
        title: 'CRON BUSINESS',
        subtitle: 'CRON BUSINESS is online cashbox, associated with a CRM',
        description: '',
        image: cronbusiness, //from public folder
        url: 'https://business.cron.ooo',
        features: [
            'Opening and closing of cash register',
            ' Management of sales, returns and Reapprovisionnement of products',
            'Search for product by name, barcode',
            'Tool for monitoring sales and products.',
            'Accounting',
        ],
        skills: [
            { url: 'https://python.org', name: 'PYTHON' },
            { url: 'https://www.djangoproject.com/', name: 'DJANGO REST FRAMEWORK' },
            { url: 'https://www.vuejs.org', name: 'VUEJS' }
        ]
    },
    {
        date: '2016 - 2018',
        description: `During this time I took a temporary break from freelancing.
        Instead, I was an employee in medium-size tech companies. This
        gave me a lot of experience and hindsight on how a proper team
        collaboration and project management process
        should look like. See <a href="/cv">my CV</a>`,
    },
    {
        date: '2014 - 2016',
        title: 'ALPHA-CLI',
        subtitle: `ALPHA-CLI is a VoIP solution integrator: implementation of
        an IP channel communication platform with the ability to
        display the caller.`,
        description: '',
        image: alphacli, //from public folder
        features: [

        ],
        skills: [
            { url: 'https://php.net', name: 'PHP' },
            { url: 'https://jquery.com', name: 'Jquery' },
            { url: 'https://dotnet.microsoft.com/apps/aspnet', name: 'ASP.NET' },
            { url: 'http://asterisk.fr', name: 'Asterisk' },
            { url: 'https://www.freepbx.org', name: 'Free PBX' },
            { url: 'https://www.zabbix.org', name: 'Zabbix' }
        ]
    },


]


export default {
    projects
}