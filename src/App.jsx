import { Router } from "@reach/router";
import Home from "./views/Home";
import NotFound from "./views/errors/404";
import Contact from "./views/Contact";
import CV from "./views/CV";
import Preview from "./views/Preview";
import './assets/main.css';

function App() {
  return <Router>
    <Home path="/" />
    <Contact path="/contact" />
    <CV path="/cv" />
    <Preview path="/preview" />
    <NotFound default />
  </Router>
}

export default App;
