import React from 'react'
import styled from 'styled-components'
import MainLayout from '../components/layouts/Main';
import { projects, assets } from '../helpers'


const Home = () => {
  return <Wrapper>
    <MainLayout>
      <MainLayout.Head>
        <title>Claver Amon</title>
      </MainLayout.Head>
      <MainLayout.Container>
        <div className="main-section portfolio-section">
          <h1 className="main-title">Portfolio</h1>

          <p>
            See my works below. Unless explicitly stated otherwise,
              <strong>this is not the exhaustive list of all my completed projects</strong>
          </p>

          <div className="timeline portfolio-timeline">
            <ul>
              {/* <!-- <li className="timeline_element timeline_element--now project">
                        <div className="timeline_element-date">
                            <time className="timeline_element-date-text">Now</time>
                        </div>

                        <div className="timeline_element-contents">
                            <div className="project-text">
                                <div className="project-description">
                                    <strong
                                    >Currently open to co-operation on new and existing
                  projects.</strong
                                    ><br />
                Looking for a CTO, a mentor, or a just full-stack dev willing to
                join your team?<br />
                                    <a href="/contact/">Contact me</a>. I will answer you in ~24
                hours.
              </div>
                            </div>
                        </div>
                    </li>--> */}
              {projects.map((project, i) => <li key={i} className="timeline_element project">
                <div className="timeline_element-date">
                  <time className="timeline_element-date-text">{project.date}</time>
                </div>

                <div className="timeline_element-contents">
                  <div className="project-text">
                    {project.title && <a href="#" className="project-link">
                      <div className="project-title">{project.title}</div>
                      {project.subtitle && <div className="project-subtitle">{project.subtitle}</div>}
                    </a>}
                    <div className="project-description">
                      {project.description && <p>
                        {/* <em >{project.description}</em> */}
                        <em dangerouslySetInnerHTML={{ __html: project.description }}></em>
                      </p>}

                      {project.features && <>
                        <p>Whole project includes:</p>
                        <ul>
                          {project.features.map((feature, i) => <li key={i}>{feature}</li>)}
                        </ul>
                      </>}
                    </div>

                    {/* <!-- <a
                                            href="https://mp.05.ru/"
                                            target="_blank"
                                            className="project-readmore button button-red"
                                        >
                                            More details
                        </a>--> */}
                    {project.url && <a href={project.url} className="button button-red" target="_blank" rel="noreferrer">
                      <i className="fa fa-external-link"></i>
                                    View online
                                </a>}
                    {project.skills && <div className="project-technologies">
                      <div className="technologies-title">Technologies</div>
                      <ul className="tech-tags">
                        {project.skills.map((skill, k) => <li key={k}>
                          <a href={skill.url} rel="nofollow" target="_blank">{skill.name}</a>
                        </li>)}
                      </ul>
                    </div>}
                  </div>
                  {project.image && <div className="project-image">
                    <a href="#">
                      <img src={project.image} alt="logo" />
                    </a>
                  </div>}
                </div>
              </li>

              )}
            </ul>
          </div>


        </div>
      </MainLayout.Container>
    </MainLayout>
  </Wrapper>
}

const Wrapper = styled.div`

`

export default Home