import Skills from "../components/cv/Skills";
import Experiences from "../components/cv/Experiences";
import Education from "../components/cv/Education";
import Layout from "../components/layouts/Base";
import portrait from '../assets/img/portrait.jpg'
const CVPage = () => {
    return <Layout>

        <Layout.Container>
            <div className="cv">
                <div className="cv-page h-resume">
                    <div className="cv-page-inner">
                        <div className="cv-section cv-header p-contact h-card">
                            <div className="cv-header-avatar">
                                <img src={portrait} alt="portrait" />
                            </div>
                            <div className="cv-header-text">
                                <h1 className="cv-header-name p-name">Claver Amon</h1>
                                <h2 className="cv-header-subname p-title">Full-stack Software Developer</h2>
                                <div className="cv-header-lead p-note">
                                    7 years of experience in back-end and front-end engineering.
                                    Ambitious and communicative. Good mentor and tech lead.
              <br />
                                </div>
                            </div>
                            <div className="cv-header-meta">
                                {/*  <div className="cv-header-meta-left">
                            <div className="cv-header-meta-row cv-header-meta-location">
                                Moscow, Leninski prospect, 150
          </div>
                        </div>*/}
                                <div className="cv-header-meta-right">
                                    <div className="cv-header-meta-row">
                                        <a href="http://portofolio.ngnia.com/" rel="me" className="u-url">portofolio</a>
                                    </div>
                                    <div className="cv-header-meta-row">
                                        <a href="mailto:ckam225@gmail.com" className="u-email">ckam225@gmail.com</a>
                                    </div>
                                    <div className="cv-header-meta-row">+7 989 871 35 79</div>
                                    <div className="cv-header-meta-row">
                                        <a href="https://linkedin.com/in/claveramon/">linkedin.com/in/claveramon</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <Skills />

                        <Experiences />

                        <Education />

                        <div className="cv-section">
                            <h3 className="cv-section-title">Other perks</h3>
                            <div className="cv-section-content cv-section-content--indented">
                                <ul>
                                    <li>Knowledge of algorithms and data structures.</li>
                                    <li>
                                        Speaks native French, conversational Russian and
                                        basic English.
              </li>
                                    <li>In his free time, write codes(algorithms challenge) dance, travel, and play football.</li>
                                </ul>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </Layout.Container>
    </Layout>
}

export default CVPage;