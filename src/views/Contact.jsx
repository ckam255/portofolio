import MainLayout from "../components/layouts/Main";

const Contact = () => {
    return (
        <MainLayout>
            <MainLayout.Head>
                <title>Claver Amon: Contact me</title>
            </MainLayout.Head>
            <MainLayout.Container>
                <div className="main-section">
                    <h1 className="main-title">Contact me</h1>

                    <p>I am available for hire and open to any ideas of cooperation.</p>

                    <div className="vcard">
                        <dl className="dl dl-vertical">
                            <dt>E-mail:</dt>
                            <dd>
                                <a href="mailto:ckam225@gmail.com"
                                ><i className="icon fa fa-envelope"></i> ckam225@gmail.com</a
                                >
                            </dd>

                            <dt>&nbsp;</dt>
                            <dd></dd>

                            <dt>LinkedIn:</dt>
                            <dd>
                                <a href="https://www.linkedin.com/in/claveramon"
                                ><i className="icon fa fa-linkedin"></i> Claver Amon</a
                                >
                            </dd>

                            <dt>Github:</dt>
                            <dd>
                                <a href="https://github.com/msi89"
                                ><i className="icon fa fa-github"></i> msi89</a
                                >
                            </dd>

                            <dt>Twitter:</dt>
                            <dd>
                                <a href="https://twitter.com/msic225"
                                ><i className="icon fa fa-twitter"></i> @msic225</a
                                >
                            </dd>
                        </dl>
                    </div>
                </div>
            </MainLayout.Container>
        </MainLayout>
    );
}

export default Contact;